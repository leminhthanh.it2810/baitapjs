function tax() {
    var name = document.getElementById("txt-name").value;
    var income = document.getElementById("txt-income").value * 1;
    var people = document.getElementById("txt-number-people").value * 1;
    var result = document.getElementById("tax");
    var tax = 0;
    if (income <= 60e+6) {
        tax = (income - 4e+6 - people * 1600000) * 0.05;
        result.innerHTML = `<h2 class="mt-5 text-success text-center">Họ tên: ${name} ;Tiền thuế thu nhập cá nhân :  ${Intl.NumberFormat().format(tax)} VND </h2>`
    } else if (income <= 120e+6) {
        tax = (income - 4e+6 - people * 1600000) * 0.1;
        result.innerHTML = `<h2 class="mt-5 text-success text-center">Họ tên: ${name} ;Tiền thuế thu nhập cá nhân :  ${Intl.NumberFormat().format(tax)} VND </h2>`
    } else if (income <= 210e+6) {
        tax = (income - 4e+6 - people * 1600000) * 0.15;
        result.innerHTML = `<h2 class="mt-5 text-success text-center">Họ tên: ${name} ;Tiền thuế thu nhập cá nhân :  ${Intl.NumberFormat().format(tax)} VND </h2>`
    } else if (income <= 384e+6) {
        tax = (income - 4e+6 - people * 1600000) * 0.2;
        result.innerHTML = `<h2 class="mt-5 text-success text-center">Họ tên: ${name} ;Tiền thuế thu nhập cá nhân :  ${Intl.NumberFormat().format(tax)} VND </h2>`
    } else if (income <= 624e+6) {
        tax = (income - 4e+6 - people * 1600000) * 0.25;
        result.innerHTML = `<h2 class="mt-5 text-success text-center">Họ tên: ${name} ;Tiền thuế thu nhập cá nhân :  ${Intl.NumberFormat().format(tax)} VND </h2>`
    } else if (income <= 960e+6) {
        tax = (income - 4e+6 - people * 1600000) * 0.3;
        result.innerHTML = `<h2 class="mt-5 text-success text-center">Họ tên: ${name} ;Tiền thuế thu nhập cá nhân :  ${Intl.NumberFormat().format(tax)} VND </h2>`
    } else {
        tax = (income - 4e+6 - people * 1600000) * 0.35;
        result.innerHTML = `<h2 class="mt-5 text-success text-center">Họ tên: ${name} ;Tiền thuế thu nhập cá nhân :  ${Intl.NumberFormat().format(tax)} VND </h2>`
    }
}

// //////////////

const USER = "user";
const COMPANY = "company";

function disableinput(select) {
    if (select.value == COMPANY) {
        document.getElementById('number-connect').style.display = "block";
    } else {
        document.getElementById('number-connect').style.display = "none";
    }
}

function calc() {
    var customer = document.getElementById("txt-customer").value;
    var inputid = document.getElementById("inputid").value * 1;
    var numberchannel = document.getElementById("number-channel").value * 1;
    var numberconnect = document.getElementById("number-connect").value * 1;
    var cab = document.getElementById("cab");
    var payment = 0;

    if (customer == USER) {
        payment = numberchannel * 7.5 + 20.5 + 4.5;
        cab.innerHTML = `<h2 class="mt-5 text-success text-center">Mã khách hàng: ${inputid} ;Tiền cáp :  ${new Intl.NumberFormat("en-US", { style: "currency", currency: "USD" }).format(payment)} </h2>`
    }

    if (customer == COMPANY) {
        if (numberconnect <= 10) {
            payment = 15 + 75 + numberchannel * 50;
            cab.innerHTML = `<h2 class="mt-5 text-success text-center">Mã khách hàng: ${inputid} ;Tiền cáp :  ${new Intl.NumberFormat("en-US", { style: "currency", currency: "USD" }).format(payment)} </h2>`
        } else {
            payment = 15 + 75 + (numberconnect - 10) * 5 + numberchannel * 50;
            cab.innerHTML = `<h2 class="mt-5 text-success text-center">Mã khách hàng: ${inputid} ;Tiền cáp :  ${new Intl.NumberFormat("en-US", { style: "currency", currency: "USD" }).format(payment)} </h2>`
        }
    }

}

