function dudoan() {
    var a = document.getElementById("txt-canh-a").value * 1;
    var b = document.getElementById("txt-canh-b").value * 1;
    var c = document.getElementById("txt-canh-c").value * 1;
    var result = document.getElementById("result");

    if (a + b > c && a + c > b && b + c > a) {
        if (a == b && b == c) {
            result.innerHTML = `<h2 class="mt-5 text-success text-center"> Là tam giác đều </h2>`
        }
        else if (a == b || a == c || b == c) {
            result.innerHTML = `<h2 class="mt-5 text-success text-center"> Là tam giác cân </h2>`
        } else if (c * c == a * a + b * b || a * a == b * b + c * c || b * b == a * a + c * c) {
            result.innerHTML = `<h2 class="mt-5 text-success text-center"> Là tam giác vuông </h2>`
        } else {
            result.innerHTML = `<h2 class="mt-5 text-success text-center"> Là tam giác bình thường </h2>`
        }
    }
}