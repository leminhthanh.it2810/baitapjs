function tinhngay() {
    var month = document.getElementById("txt-thang").value * 1;
    var year = document.getElementById("txt-nam").value * 1;
    var result = document.getElementById("result");

    if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12 ){
        result.innerHTML = `<h2 class="mt-5 text-success text-center"> tháng ${month} năm ${year} có 31 ngày </h2>`
        
    }else if(month == 4 || month == 6 || month == 9 || month == 11 ){
        result.innerHTML = `<h2 class="mt-5 text-success text-center"> tháng ${month} năm ${year} có 30 ngày </h2>`
    }else{
        if(year % 4 == 0 && year % 100 != 0){
            result.innerHTML = `<h2 class="mt-5 text-success text-center"> tháng 2 năm ${year} có 29 ngày </h2>`
        }else{
            result.innerHTML = `<h2 class="mt-5 text-success text-center"> tháng 2 năm ${year} có 28 ngày </h2>`
        }
    }
}