function docso() {
    var number = document.getElementById("txt-so").value * 1;
    var result = document.getElementById("result");

    var hundred, ten, unit;
    hundred = Math.floor(number / 100);
    console.log("🚀 ~ file: index.js:7 ~ docso ~ hundred", hundred)
    ten = Math.floor(number % 100 / 10);
    console.log("🚀 ~ file: index.js:9 ~ docso ~ ten", ten)
    unit = number % 10;
    console.log("🚀 ~ file: index.js:11 ~ docso ~ unit", unit)

    var tram, le, chuc, donvi;

    switch (hundred) {
        case 1:
            tram = result.innerHTML = ` một trăm `
            break;
        case 2:
            tram = result.innerHTML = ` hai trăm `
            break;
        case 3:
            tram = result.innerHTML = ` ba trăm `
        case 4:
            tram = result.innerHTML = ` bốn trăm `
            break;
        case 5:
            tram = result.innerHTML = ` năm trăm `
            break;
        case 6:
            tram = result.innerHTML = ` sáu trăm `
            break;
        case 7:
            tram = result.innerHTML = ` bảy trăm `
            break;
        case 8:
            tram = result.innerHTML = ` tám trăm `
            break;
        case 9:
            tram = result.innerHTML = ` chín trăm `
            break;
        default:
            tram = result.innerHTML = ``
    }

    if (ten % 10 == 0 && unit != 0) {
        le = result.innerHTML = ` lẻ `
    } else {
        le = result.innerHTML = ``
    }

    switch (ten) {
        case 1:
            chuc = result.innerHTML = ` mười `
            break;
        case 2:
            chuc = result.innerHTML = ` hai mươi `
            break;
        case 3:
            chuc = result.innerHTML = ` ba mươi `
        case 4:
            chuc = result.innerHTML = ` bốn mươi `
            break;
        case 5:
            chuc = result.innerHTML = ` năm mươi `
            break;
        case 6:
            chuc = result.innerHTML = ` sáu mươi `
            break;
        case 7:
            chuc = result.innerHTML = ` bảy mươi `
            break;
        case 8:
            chuc = result.innerHTML = ` tám  mươi`
            break;
        case 9:
            chuc = result.innerHTML = ` chín mươi `
            break;
        default:
            chuc = result.innerHTML = ``
    }

    switch (unit) {
        case 1:
            donvi = result.innerHTML = ` một `
            break;
        case 2:
            donvi = result.innerHTML = ` hai `
            break;
        case 3:
            donvi = result.innerHTML = ` ba `
            break;
        case 4:
            donvi = result.innerHTML = ` bốn `
            break;
        case 5:
            donvi = result.innerHTML = ` năm `
            break;
        case 6:
            donvi = result.innerHTML = ` sáu `
            break;
        case 7:
            donvi = result.innerHTML = ` bảy `
            break;
        case 8:
            donvi = result.innerHTML = ` tám `
            break;
        case 9:
            donvi = result.innerHTML = ` chín `
            break;
        default:
            donvi = result.innerHTML = ``
    }

    result.innerHTML = `<h2 class="mt-5 text-success text-center "> ${tram} ${le} ${chuc} ${donvi} </h2>`
}