function ngayhomqua() {
    var day = document.getElementById("txt-ngay").value * 1;
    var month = document.getElementById("txt-thang").value * 1;
    var year = document.getElementById("txt-nam").value * 1;
    var result = document.getElementById("result");

    result.innerHTML = `<h2 class="mt-5 text-success text-center"> ${day - 1}/${month}/${year} </h2>`

    if (day == 1) {
        if (month == 5 || month == 7 || month == 10 || month == 12) {
            result.innerHTML = `<h2 class="mt-5 text-success text-center"> ${30}/${month - 1}/${year} </h2>`
        }
        else if (month == 1 || month == 2 || month == 4 || month == 6 || month == 8 || month == 9 || month == 11) {
            if (month == 1) {
                result.innerHTML = `<h2 class="mt-5 text-success text-center"> ${31}/${12}/${year - 1} </h2>`
            } else {
                result.innerHTML = `<h2 class="mt-5 text-success text-center"> ${31}/${month - 1}/${year} </h2>`
            }
        }
        else if (month == 3) {
            if (year % 4 == 0 && year % 100 != 0) {
                result.innerHTML = `<h2 class="mt-5 text-success text-center"> ${29}/${month - 1}/${year} </h2>`
            }
            else {
                result.innerHTML = `<h2 class="mt-5 text-success text-center"> ${28}/${month - 1}/${year} </h2>`
            }
        }
        else {
            result.innerHTML = `<h2 class="mt-5 text-success text-center"> ${day - 1}/${month}/${year} </h2>`
        }
    }
}

function ngaymai() {
    var day = document.getElementById("txt-ngay").value * 1;
    var month = document.getElementById("txt-thang").value * 1;
    var year = document.getElementById("txt-nam").value * 1;
    var result = document.getElementById("result");


    result.innerHTML = `<h2 class="mt-5 text-success text-center"> ${day + 1}/${month}/${year} </h2>`

    if (day == 31) {
        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
            if (month == 12) {
                result.innerHTML = `<h2 class="mt-5 text-success text-center"> ${1}/${1}/${year + 1} </h2>`
            }
            else {
                result.innerHTML = `<h2 class="mt-5 text-success text-center"> ${1}/${month + 1}/${year} </h2>`
            }
        }
    }
    else if (day == 30) {
        if (month == 4 || month == 6 || month == 9 || month == 11) {
            result.innerHTML = `<h2 class="mt-5 text-success text-center"> ${1}/${month + 1}/${year} </h2>`
        }
    }
    else if (month == 2) {
        if (year % 4 == 0 && year % 100 != 0) {
            if (day == 29) {
                result.innerHTML = `<h2 class="mt-5 text-success text-center"> ${1}/${3}/${year} </h2>`
            }else{
                result.innerHTML = `<h2 class="mt-5 text-success text-center"> ${day + 1}/${month}/${year} </h2>`
            }
        }
        else if (day == 28) {
            result.innerHTML = `<h2 class="mt-5 text-success text-center"> ${1}/${3}/${year} </h2>`
        }
        else {
            result.innerHTML = `<h2 class="mt-5 text-success text-center"> ${day + 1}/${month}/${year} </h2>`
        }
    }
}
