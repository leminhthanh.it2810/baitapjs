function outcome() {
    var area = document.getElementById("txt-area").value * 1;
    var object = document.getElementById("txt-object").value * 1;
    var scoredefault = document.getElementById("txt-score-default").value * 1;
    var score1 = document.getElementById("txt-score-1").value * 1;
    var score2 = document.getElementById("txt-score-2").value * 1;
    var score3 = document.getElementById("txt-score-3").value * 1;
    var result = document.getElementById("result");
    var finalscore = score1 + score2 + score3 + area + object;

    if (finalscore >= scoredefault) {
        if (score1 == 0 || score2 == 0 || score3 == 0) {
            result.innerHTML = `<h2 class="mt-5 text-success text-center"> Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0 </h2>`
        }
        else {
            result.innerHTML = `<h2 class="mt-5 text-success text-center"> Bạn đã đậu. Tổng điểm của bạn: ${finalscore} </h2>`
        }
    } else {
        result.innerHTML = `<h2 class="mt-5 text-success text-center"> Bạn đã rớt. Tổng điểm của bạn: ${finalscore} </h2>`
    }
}

// ////////////

var payment = 0;
var giatien = 0;

function tinhgiatien50kwdau() {
    return 500;
}

function tinhgiatien50kwtiep() {
    return 650;
}

function tinhgiatien100wtiep() {
    return 850;
}
function tinhgiatien150kwtiep() {
    return 1100;
}
function tinhgiatienkwconlai() {
    return 1300;
}

function receipt() {
    var name = document.getElementById("txt-name").value;
    var numberkw = document.getElementById("txt-number-kw").value * 1;
    var receipt = document.getElementById("receipt");

    var giatien50kwdau = tinhgiatien50kwdau();
    var giatien50kwtiep = tinhgiatien50kwtiep();
    var giatien100wtiep = tinhgiatien100wtiep();
    var giatien150kwtiep = tinhgiatien150kwtiep();
    var giatienkwconlai = tinhgiatienkwconlai();

    if (numberkw <= 50) {
        payment = giatien50kwdau * numberkw;
    } else if (numberkw <= 100) {
        payment = giatien50kwdau * 50 + giatien50kwtiep * (numberkw - 50);
    } else if (numberkw <= 200) {
        payment = giatien50kwdau * 50 + giatien50kwtiep * 50 + giatien100wtiep * (numberkw - 100);
    } else if (numberkw <= 350) {
        payment = giatien50kwdau * 50 + giatien50kwtiep * 50 + giatien100wtiep * 100 + giatien150kwtiep * (numberkw - 200);
    } else {
        payment = giatien50kwdau * 50 + giatien50kwtiep * 50 + giatien100wtiep * 100 + giatien150kwtiep * 150 + giatienkwconlai * (numberkw - 350);
    }

    receipt.innerHTML = `<h2 class="mt-5 text-success text-center"> Họ tên: ${name}; Tiền điện: ${Intl.NumberFormat().format(payment)} VND </h2>`

}