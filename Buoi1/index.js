/**
 * bài 1: tính tiền lương nhân viên
 * đầu vào: lương 1 ngày, số ngày làm
 * các bước xử lý:
 * b1: tạo 2 biến và gán giá trị cho lương 1 ngày và số ngày làm là luong1 và songay
 * b2: tạo biến tiền lương là TL
 * b3: dùng công thức TL = luong1 * songay
 * b4: in kết quả ra console
 * đầu ra: tiền lương
 */
var luong1 = 100000, songay = 30;
var TL;
TL = luong1 * songay
console.log("🚀 ~ file: index.js ~ line 14 ~ TL", TL) //TL = 3 000 000

/**
 * bài 2: tính giá trị trung bình
 * đầu vào: 5 số thực
 * các bước xử lý:
 * b1: tạo 5 biến và gán giá trị cho 5 số thực là st1, st2, st3, st4, st5
 * b2: tạo biến giá trị trung bình là TB
 * b3: dùng công thức TB= (st1 +st2 +st3 +st4+ st5)/5
 * b4: in kết quả ra console
 * đầu ra: giá trị trung bình
 */
var st1 = 1, st2 = 2, st3 = 3, st4 = 4, st5 = 5;
var TB;
TB = (st1 + st2 + st3 + st4 + st5) / 5
console.log("🚀 ~ file: index.js ~ line 29 ~ TB", TB) //TB = 3

/**
 * bài 3: quy đổi tiền
 * đầu vào: giá 1 usd, số tiền đổi
 * các bước xử lý:
 * b1: tạo 2 biến và gán giá trị cho giá 1 usd và số tiền đổi là usd và std
 * b2: tạo biến số tiền quy đổi là stqd
 * b3: dùng công thức stqd = usd * std
 * b4: in kq ra console
 * đầu ra: số tiền quy đổi
 */
var usd = 23500, std = 100;
var stqd;
stqd = usd * std
console.log("🚀 ~ file: index.js ~ line 44 ~ stqd", stqd) // stqd = 2 350 000

/**
 * bài 4: tính diện tích, chu vi hình chữ nhật
 * đầu vào: chiều dài, chiều rộng
 * các bước xử lý:
 * b1: tạo 2 biến và gán giá trị cho chiều dài và chiều rộng là cd, cr
 * b2: tạo 2 biến cho diện tích và chu vi là S, H
 * b3: dùng công thức S = cd * cr, H = (cd + cr) * 2
 * b4: in kết quả ra console
 * đầu ra: diện tích, chu vi
 */
var cd = 10, cr = 5;
var S, H;
S = cd * cr
console.log("🚀 ~ file: index.js ~ line 59 ~ S", S) // S = 50
H = (cd + cr) * 2
console.log("🚀 ~ file: index.js ~ line 61 ~ H", H) // H = 30

/**
 * bài 5: tính tổng 2 ký số
 * đầu vào: 1 số có 2 chữ số
 * các bước xử lý:
 * b1: tạo biến và gán trị cho 1 số có 2 chữ số là n
 * b2: tạo 3 biến cho số hàng chục, số hàng đơn vị và tổng 2 ký số là ten, unit, tong
 * b3: dùng công thức ten = Math.floor(n / 10), unit = n % 10, tong = ten + unit
 * b4: in kết quả ra console
 * đầu ra: tổng 2 ký số
 */
var n = 58;
var ten, unit;
ten = Math.floor(n / 10);
unit = n % 10;
tong = ten + unit;
console.log("🚀 ~ file: index.js ~ line 78 ~ tong", tong) //tong = 13
